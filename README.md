# Tour guide
## To install
### Prerequisites
- java 11 : https://www.oracle.com/java/technologies/downloads/#license-lightbox
- gradle (installed with IDE)
- git (installed with IDE)

### Steps
- clone repository
`git clone https://github.com/simoncourtecuisse/TourGuide.git`

- create gradle configuration in IDE for build/run


